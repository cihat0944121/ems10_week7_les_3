/**
 * @file clocks.h
 * @brief Header file voor alle klokdefinities en functies
 * @Author: VersD
 * @date 4 mei 2017
 */

#ifndef LIB_CLOCKS_H_
#define LIB_CLOCKS_H_

#include <msp430.h>
#include <stdint.h>
/**
 * \defgroup CLOCK
 * @brief Bevat een aantal functies om het kloksysteem van de microcontroller in te stellen
 *
 * Er zijn 3 systeemklokken:
 * - ACLK
 * - SMCLK
 * - MCLK
 *
 * Deze systeemklokken worden afgeleid van verschillende oscillatoren
 * - LFXT1: Als een horloge-kristal is aangesloten
 * - VLO: De interne 12khz oscillator
 * - DCO: De interne Digital Controlled Oscillator
 *
 * Vaak is er ook een deling van de oscillator frequentie mogelijk.
 *
 * @{
 *
 */

/*!
 * De opties voor de klokbronnen.
 */
typedef enum {PERIPHERAL=0, ACLK, SMCLK, MCLK} clockSources;

/*!
 * De opties voor de oscillatoren
 */
typedef enum {VLO, DCO, LFXT1} Oscillator;

/*!
 * @brief Configureer de SMCLK naar een bepaalde oscillator met een deler
 *
 * \param bron De te gebruiken oscillator
 * \param deler De te gebruiken deler. Mogelijkheden zijn 1,2,4,8
 */
void clkConfigureSMCLK(Oscillator bron, uint8_t deler);

/*!
 * @brief Configureer de ACLK naar een bepaalde oscillator met een deler
 *
 * \param bron Kiest tussen LFXT1 en de VLO. Slechts 1 optie mogelijk voor de hele uc, overschrijft eerdere keuzes.
 * \param deler De te gebruiken deler. Mogelijkheden zijn 1,2,4,8
 */
void clkConfigureACLK(Oscillator bron, uint8_t deler);

/*!
 * @brief Configureer de MCLK naar een bepaalde oscillator met een deler
 *
 * \param bron Kiest tussen LFXT1 en de VLO. Slechts 1 optie mogelijk voor de hele uc, overschrijft eerdere keuzes.
 * \param deler De te gebruiken deler. Mogelijkheden zijn 1,2,4,8
 */
void clkConfigureMCLK(Oscillator bron, uint8_t deler);

/*!
 * @brief Start een bepaalde oscillator
 *
 * \param bron Kiest tussen LFXT1 en de VLO. Slechts 1 optie mogelijk voor de hele uc, overschrijft eerdere keuzes.
 */
void clkStartOscillator(Oscillator bron);

/*!
 * @brief Beperkte log2 implementatie.
 *
  */
uint8_t clkGetDivision(uint8_t deler);

/**
 * @}
 */
#endif /* LIB_CLOCKS_H_ */
